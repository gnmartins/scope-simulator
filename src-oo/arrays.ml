open Core;;

class ['a] _array ?(init = [| |]) () = object (self)
    val mutable items : 'a array = init

    method length = Array.length items

    method get_items = items
    
    method get (pos : int) = items.(pos)

    method update (pos : int) (item : 'a) : unit = items.(pos) <- item

    method push (item : 'a) : unit = items <- Array.append items [| item |]
    
    method pop : 'a = 
        let last_idx = self#length - 1 in
        let _items = Array.slice items 0 last_idx in
        let popped = self#get last_idx in 
        items <- _items; popped 

    method slice (s : int) (e : int) : 'a _array = 
        new _array ~init:(Array.slice items s e) () 
        
    method print_formatted (to_string : 'a -> string) =
        Array.iter ~f:(fun x -> print_string (to_string x)) items

end