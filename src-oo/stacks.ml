open Core
open Arrays

exception UndeclaredVariableError;;

type var = 
  | VARIABLE of string * int       (* identificador, valor *)
  | FUNCTION of string * int * int (* identificador, inicio, pai estatico *)

type scopes = 
  | DYNAMIC
  | STATIC

(* Retorna variável a partir de uma `option` *)
let get_var_content x = 
  match x with
  | Some v -> v
  | None -> raise UndeclaredVariableError;; 

class virtual _var id def = object
  val id : string = id
  val def : int = def

  method get_id : string = id
  method get_def : int = def
  method virtual get_content : var
  method virtual print : unit
end 

class variable_var id def value = object
  inherit _var id def
  val value : int = value

  method get_content = (VARIABLE (id, value))
  method print = print_string (sprintf "\tvar %s = %d\n" id value)
end

class function_var id def start ve = object
  inherit _var id def 
  val start : int = start
  val ve : int = ve

  method get_content = (FUNCTION (id, start, ve))
  method print = print_string (sprintf "\tfunction %s\n" id)
end

(* Frame *)
class frame ret_ ve_ vd_ = object 
  val ret : int = ret_
  val ve : int = ve_
  val vd : int = vd_
  val mutable vars : _var _array = new _array ()

  method get_ret = ret
  method get_ve = ve
  method get_vd = vd
  method get_vars = vars

  (* Adiciona variável ao frame *)
  method set_var (v : _var) : unit = vars#push v
  (* Retorna a variável com identificador `id` ou None *)
  method get_var (id : string) : _var option = 
    let found = ref false in
    let i = ref 0 in
    while !i < vars#length && not !found do 
      if (vars#get !i)#get_id = id then found := true;
      if not !found then i := !i + 1
    done;
    if !i < vars#length then (
      Some (vars#get !i)
    ) else None

  (* Imprime o frame *)
  method print : unit= 
    print_string (sprintf "\n------------------------\n");
    for i = vars#length - 1 downto 0 do
      (vars#get i)#print
    done;
    print_string (sprintf "VD\t%d\n" vd);
    print_string (sprintf "VE\t%d\n" ve);
    print_string (sprintf "RET\t%d\n" ret);
    print_string (sprintf "------------------------")
end

(* Representação abstrata de uma pilha *)
class virtual _stack = object
  val mutable frames : frame _array = 
    let f = new frame (-1) (-1) (-1) in 
    (new _array ~init:[|f|] ())
    
  method n_of_frames : int = frames#length
  method virtual get_var : string -> _var

  (* Adiciona variável ao último frame *)
  method set_var (var : var) (def : int) : unit = 
    let idx = (frames#length - 1) in
    let f = ref (frames#get idx) in
    let variable = (match var with 
      | VARIABLE (id, value) -> new variable_var id def value
      | FUNCTION (id, start, ve) -> new function_var id def start ve
    ) in
    !f#set_var variable;
    frames#update idx !f

  (* Empilha novo frame *)
  method push_frame (ret : int) (ve : int) : unit =
    let vd = (frames#length) - 1 in
    frames#push (new frame ret ve vd)

  (* Desempilha frame e retorna endereço de retorno *)
  method pop_frame : int = 
    let popped = frames#pop in
    popped#get_ret

  (* Imprime a pilha *)
  method print : unit = 
    for i = frames#length - 1 downto 0 do
      (frames#get i)#print
    done
end

(* Pilha para escopo dinâmico *)
class dstack = object 
  inherit _stack

  (* Retorna a variável `id` a partir de escopo dinâmico *)
  method get_var (id : string) : _var = 
    let i = ref (frames#length - 1) in
    let var = ref None in 
    while !var = None && !i >= 0 do
      var := (frames#get !i)#get_var id;
      if !var = None then i := (frames#get !i)#get_vd
    done;
    get_var_content !var
end

(* Pilha para escopo estático *)
class sstack = object 
  inherit _stack

  (* limitante para busca de variável 
     se a variável encontrada tiver sido definida em linha após
     o a linha limitante, então ela deve ser desconsiderada *)
  val mutable limiter : int = -1 
  method set_limiter (limit : int) : unit = (limiter <- limit) 

  (* Retorna a variável `id` a partir de escopo estático *)
  method get_var (id : string) : _var = 
    let i = ref (frames#length - 1) in
    let var = ref None in 
    while !var = None do
      var := (frames#get !i)#get_var id;
      if !var = None then i := (frames#get !i)#get_ve
    done;
    let v = get_var_content !var in
    (* se a variável foi definida em linha após a chamada da função, 
       ela não deve ser acessível *)
    if (v#get_def < limiter)
    then (v) else (raise UndeclaredVariableError)
end

(* Delegate para pilhas *)
class stack scope = object (self)

  val scope = (match String.lowercase scope with
  | "s" | "static" -> STATIC
  | "d" | "dynamic" -> DYNAMIC
  | _ -> DYNAMIC)
  val ds = new dstack
  val ss = new sstack

  method get_scope : scopes = scope

  method private get_var (var : string) (line : int) : _var = 
    match scope with
    | STATIC -> ss#set_limiter line; ss#get_var var
    | DYNAMIC -> ds#get_var var

  method n_of_frames : int =
    match scope with
    | STATIC -> ss#n_of_frames
    | DYNAMIC -> ds#n_of_frames

  method set_variable (id : string) (def : int) (value : int) : unit =
    let variable = VARIABLE (id, value) in
    match scope with
    | STATIC -> ss#set_var variable def
    | DYNAMIC -> ds#set_var variable def

  method get_variable (id : string) (line : int) : int option =
    let var = self#get_var id line in
    match var#get_content with
    | VARIABLE (_, value) -> Some value
    | _ -> None

  method set_function (id : string) (def : int) (start : int) (pe : int) : unit =
    let f = FUNCTION (id, start, pe) in
    match scope with 
    | STATIC -> ss#set_var f def
    | DYNAMIC -> ds#set_var f def

  method get_function (id : string) (line : int) =
    let var = self#get_var id line in
    match var#get_content with
    | FUNCTION (_, start, ve) -> Some (start, ve)
    | _ -> None

  method push_frame (ret : int) (ve : int) : unit =
    match scope with
    | STATIC -> ss#push_frame ret ve
    | DYNAMIC -> ds#push_frame ret ve

  method pop_frame : int =
    match scope with
    | STATIC -> ss#pop_frame
    | DYNAMIC -> ds#pop_frame

  method print : unit =
    match scope with
    | STATIC -> ss#print
    | DYNAMIC -> ds#print
end