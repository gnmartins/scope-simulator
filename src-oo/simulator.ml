open Core;;
open Interpreter;;

let dumb_cls () = print_string "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"

let run (filename : string) (scopetype : string) : unit = 
    let interp = (new interpreter filename scopetype) in
    while (not interp#finished) do
        dumb_cls ();
        interp#print_stack;
        print_string "\n";
        interp#print_program;
        print_string "\n";
        interp#print_outputs;
        Out_channel.(flush stdout); 
        ignore In_channel.(input_line_exn stdin);
        interp#run_next_line
    done

let command = 
  Command.basic
    ~summary: "Interpreter for a simple language with dynamic or static scope"
    Command.Spec.(
      empty
      +> anon ("filename" %: string)
      +> anon ("scopetype" %: string)
    )
    (fun filename scopetype () ->
      run filename scopetype
    )

let () =
  Command.run ~version:"1.0" ~build_info:"MLP_v1.0" command;;
