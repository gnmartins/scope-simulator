open Core

exception ParsingError;;

type operator =
  | PLUS
  | MINUS;;

type command =
  | LET
  | ASSIGN
  | PRINT
  | FUNCSTART
  | FUNCEND
  | CALL;;

type token = 
  | INT of int
  | OP of operator
  | VAR of string
  | COMMAND of command
  | EOF;;

class parser s = object (self)
  val mutable str : string = s
  val mutable pos : int = 0
  val mutable buffer : string = ""
  val mutable len : int = String.length s

  method get_pos = pos
  method get_str = str

  (* Seta nova string e reseta a posição *)
  method set_str (s : string) : unit = 
    str <- s;
    pos <- 0;
    len <- String.length s

  (* Retorna um inteiro na `str` a partir de `pos` *)
  method private get_int : int =
    let start = pos in
    while pos < len && Char.is_digit (String.nget str pos) do
      pos <- pos + 1
    done;
    let substr = String.slice str start pos in
    int_of_string substr

  (* Retorna substring até a partir de `pos` até próximo whitespace *)
  method private get_substr : string =
    let start = pos in
    while pos < len && (String.nget str pos) <> ' ' do
      pos <- pos + 1
    done;
    String.slice str start pos

  (* Vai até o próximo caractere que não seja whitespace *)
  method private skip_whitespace : unit=
    while pos < len && (String.nget str pos) = ' ' do 
      pos <- pos + 1
    done

  (* Pega o próximo token presente em `str` *)
  method get_next_token : token =
    self#skip_whitespace;
    if pos >= len then (
      EOF
    ) else (
      (* testando se é um número *)
      let c = String.nget str pos in
      if Char.is_digit c then (
        INT self#get_int
      ) else (
        (* testando se é um operador *)
        match c with
        | '+' -> pos <- pos + 1; OP PLUS
        | '-' -> pos <- pos + 1; OP MINUS
        | '=' -> pos <- pos + 1; COMMAND ASSIGN
        | _ -> (
          (* testando outros comandos *)
          let s = self#get_substr in
          match s with
          | "let" -> COMMAND LET
          | "print" -> COMMAND PRINT
          | "function" -> COMMAND FUNCSTART 
          | "end" -> COMMAND FUNCEND
          | "call" -> COMMAND CALL
          | _ -> VAR s
        )
      ) 
    )

  (* Retorna list com todos os tokens presentes em `str` *)
  method get_tokens : token list = 
    pos <- 0;
    let tokens : token list ref = ref [] in
    while pos < len do
      let next_token = self#get_next_token in
      tokens := !tokens @ [next_token]
    done;
    tokens := !tokens @ [EOF];
    !tokens
end