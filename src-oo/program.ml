open Core;;
open Core_kernel__.Import;;

class program filename = object
  val lines : string array = List.to_array (In_channel.read_lines filename)
  val mutable curr : int = 0

  method go_to (n : int) : unit = curr <- n
  method get_line_number : int = curr

  method get_line : string = 
    if curr < Array.length lines then (
      curr <- curr + 1; lines.(curr - 1) 
    ) else "EOF"
  
  method print_full : unit = 
    for i = 0 to Array.length lines - 1 do
      let mark = if (i = curr) then ("*") else (" ") in
      print_string (sprintf "%s %d\t%s\n" mark i lines.(i))
    done
  
  method print : unit = 
    let start = if curr > 0 then (curr - 1) else curr in
    for i = start to start + 3 do
      let mark = if (i = curr) then ("*") else (" ") in
      if i < Array.length lines then 
        print_string (sprintf "%s %d\t%s\n" mark i lines.(i))
      else if i = Array.length lines then 
        print_string (sprintf "%s %d\t\n" mark i)
    done
end