open Core;;
open Parser;;
open Stacks;;
open Program;;

exception RuntimeError;;
exception SyntaxError of string;;

class interpreter filename scope = object (self)
  val program = new program filename
  val parser = new parser ""
  val stack = new stack scope

  val mutable outputs : string list = []
  val mutable ended : bool = false

  val mutable current_line : int = 0
  val mutable ans : int option = None 

  method finished = ended
  method get_outputs = outputs
  method get_current_line = current_line
  method get_ans = 
    (match ans with
    | Some x -> x
    | None -> raise RuntimeError)

  method private get_next_line : string =
     current_line <- program#get_line_number;
     program#get_line

  method private throw_error = raise (SyntaxError (sprintf "line %d" current_line)) 

  method private operate (x : int) (op : operator) (y : int) : int =
    match op with
    | PLUS -> x + y
    | MINUS -> x - y

  method private get_function_end : int = 
    let nest = ref 0 in
    let f_end = ref (-1) in
    while !f_end = -1 do
      let line = self#get_next_line in
      if line = "EOF" then self#throw_error else (
        parser#set_str line;
        match parser#get_next_token with
        | COMMAND FUNCEND -> ( 
          if !nest = 0 then (f_end := current_line) else (nest := !nest - 1)
        )
        | COMMAND FUNCSTART -> nest := !nest + 1
        | _ -> ()
      )
    done; !f_end

  method private evaluate (tokens : token list) : unit =
    match tokens with
    | hd :: tl -> (
      match hd with
      | INT x -> (
        match ans with
        (* se ans <> None significa algo tipo `10 20` *)
        | None -> (ans <- Some x); self#evaluate tl
        | _ -> self#throw_error
      )
      | VAR x -> (
        match ans with
        | None -> (
          let var = stack#get_variable x current_line in
          match var with
          | Some value -> (ans <- Some value); self#evaluate tl
          | None -> self#throw_error
        )
        | _ -> self#throw_error
      )
      | OP op -> (
        let x = (
          match ans with 
          | Some v -> v
          | None -> 0
        ) in
        match tl with 
        | INT y :: tl' -> (ans <- Some (self#operate x op y); self#evaluate tl') 
        | VAR v :: tl' -> (
          let var = stack#get_variable v current_line in
          match var with
          | Some y -> (ans <- Some (self#operate x op y); self#evaluate tl')
          | None -> self#throw_error
        )
        | _ -> self#throw_error
      )
      | COMMAND x -> (
        match x with
        | LET -> (
          match tl with
          (* `let var = expr` *)
          | VAR v :: COMMAND ASSIGN :: tl' -> (
            (* avalia expr e seta variável na pilha com resultado *)
            self#evaluate tl';
            stack#set_variable v current_line self#get_ans
          )
          | _ -> self#throw_error
        )
        | PRINT -> (
          (* avalia resto da expressão e imprime resultado *)
          self#evaluate tl;
          match ans with
          | Some v -> (outputs <- outputs @ [(sprintf "%d" v)])
          | None -> self#throw_error
        )
        | CALL -> (
          match tl with
          (* linha deve ser `call f` *)
          | VAR f :: EOF :: _ -> (
            let f = stack#get_function f current_line in
            let (fstart, ve) = (
              match f with
              | Some func -> func
              | None -> self#throw_error; (-1, -1)
            ) in
            stack#push_frame (current_line+1) ve;
            program#go_to fstart
          )
          | _ -> self#throw_error
        )
        | FUNCSTART -> (
          match tl with
          (* linha deve ser `function f` *)
          | VAR f :: EOF :: _ -> (
            let f_def = current_line in
            let _ = self#get_function_end in
            let pe = stack#n_of_frames - 1 in
            stack#set_function f f_def (f_def+1) pe
          )
          | _ -> self#throw_error
        )
        | FUNCEND -> (
          let ret = stack#pop_frame in
          program#go_to ret
        )
        | _ -> self#throw_error
      )
      | EOF -> ()
    )
    | _ -> self#throw_error

  method run_next_line : unit = 
    ans <- None;
    let line = self#get_next_line in
    if line = "EOF" then (
      ended <- true
    ) else (
      parser#set_str line;
      let tokens = parser#get_tokens in
      self#evaluate tokens
    )

  method run_line (line : string) : unit =
    ans <- None;
    parser#set_str line;
    let tokens = parser#get_tokens in 
    self#evaluate tokens

  method print_stack : unit = stack#print
  method print_program : unit = program#print
  method print_outputs : unit = 
    List.iter ~f:(fun x -> 
      print_string (sprintf "%s\n" x)
    ) outputs

end