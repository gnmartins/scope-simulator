# scope-simulator

Static and dynamic scope simulator for a simple pseudo-language.

```
let x = 10

function f
    let z = x + 5
    print z + 1
end

function g
    let x = 20
    call f
end

call g
```
