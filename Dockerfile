FROM ocaml/opam:ubuntu

COPY .ocamlinit /home/opam/.ocamlinit

ENTRYPOINT ["/bin/bash"]

RUN opam depext -i core
RUN opam depext -i utop
RUN opam depext -i menhir
RUN opam depext -i merlin

RUN /bin/bash -c "eval `opam config env`"
