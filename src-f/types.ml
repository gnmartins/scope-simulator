exception RuntimeError;;
exception ParsingError;;
exception SyntaxError of string;;
exception UndeclaredVariableError;;

type operator =
  | PLUS
  | MINUS;;

type command =
  | LET
  | ASSIGN
  | PRINT
  | FUNCSTART
  | FUNCEND
  | CALL;;

type token = 
  | INT of int
  | OP of operator
  | VAR of string
  | COMMAND of command
  | EOF;;
(* ==================================================================================== *)

