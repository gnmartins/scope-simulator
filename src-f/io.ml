open Core;;
open Types;;
open Stacks;;
open Helpers;;

(* Lista para armazenas outputs do simulador *)
let outputs = ref [];;
(* Armazena `x` nos outputs do simulador *)
let print x = 
  outputs := !outputs @ [x];
  print_string x;;

(* Imprime a lista `lst` baseada na formatação dada por `f` *)
let print_list ~(f : 'a -> string) (lst : 'a list) =
  List.iter ~f:(fun x ->
    print_string (f x)
  ) lst

(* Imprime o programa, colocando um identificador na linha `tgt`,
   que corresponde a próxima linha a ser executada 
let rec print_full_program (program : string list) (tgt : int) (line : int) =
  match program with
  | hd :: tl ->
    let id = (if tgt = line then "*" else " ") in
    print_string (sprintf "%s %d\t%s\n" id line hd);
    print_program tl tgt (line+1)
  | _ -> ();;
*)

(* Imprime até 4 linhs do programa, colocando um identificador na linha `tgt`,
   que corresponde a próxima linha a ser executada *)
let rec print_program ?(max = 0) (program : string list) (tgt : int) (line : int) =
  (* definindo limites e inicio de print 
     se max=0, então é a primeira recrusão: definir início e fim *)
  let (newmax, curr) = if max <> 0 then (max, line) else (
    match tgt with 
    | 0 -> (tgt+3, 0)
    | _ -> (tgt+3, tgt-1)   
  ) in
  (* se o número de linha `line` está dentro do limite, imprime 
     caso `line` > tamanho do programa, imprime linha vazia *)
  if line <= newmax then (
    let id = (if tgt = line then "*" else " ") in
    let stropt = List.nth program curr in
    match stropt with
    | Some str -> (
      print_string (sprintf "%s %d\t%s\n" id curr str);
      print_program ~max:newmax program tgt (curr+1)
    )
    | _ -> print_string (sprintf "%s %d\t\n" id curr)
  ) 
  
(* Imprime uma lista de variáveis *)
let print_variables (vars : variable list) = 
  print_list ~f:(fun v ->
    match v with 
    | VARIABLE (id, value, _) -> (sprintf "\tvar %s = %d\n" id value)
    | FUNCTION (id, _, _) -> (sprintf "\tfunction %s\n" id)
  ) vars

(* Imprime recursivamente a lista de frames a partir da posição `pos` até 0 *)
let rec _print_stack (stack : frame list) (pos : int) = 
  if pos < 0 then () else (
    let frame = get_content (List.nth stack pos) in
    print_string (sprintf "\n------- frame %d -------\n" pos);
    print_variables (List.rev frame.vars);
    print_string (sprintf "VD\t%d\n" frame.vd);
    print_string (sprintf "VE\t%d\n" frame.ve);
    print_string (sprintf "RET\t%d\n" frame.ret);
    print_string (sprintf "------------------------");
    _print_stack stack (pos - 1));;

let print_stack stack = _print_stack stack ((List.length stack) - 1);;

(* Imprime pilha, programa e outputs do simulador, depois aguarda por qualquer input
   do usuário antes de terminar a execução *)
let print_and_wait stack program curr_line =
  print_string "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
  print_stack stack;
  print_string "\n";
  print_program program curr_line 0;
  print_string "\n";
  print_list ~f:(fun x -> sprintf "%s\n" x) !outputs;
  Out_channel.(flush stdout); 
  ignore In_channel.(input_line_exn stdin);;