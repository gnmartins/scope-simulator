open Core;;
open Io;;
open Types;;
open Helpers;;
open Stacks;;
open Parser;;
open Interpreter;;

(* A partir dos tokens de uma linha, retorna `true` se a mesma corresponde 
  a definição de uma função *)
let is_function_definition tokens line =
  match tokens with
  | COMMAND FUNCSTART :: VAR _ :: EOF :: [] -> true
  | COMMAND FUNCSTART :: _ -> raise (SyntaxError (sprintf "line %d" line))
  | _ -> false;;

(* Retorna o identificador da função declarada a partir de `tokens` *)
let get_function_id tokens =
  match tokens with 
  | COMMAND FUNCSTART :: VAR v :: _ -> v
  | _ -> raise RuntimeError;;

(* Retorna a linha correspondente ao token que indica término de definição de função*)
let rec get_function_end program line_number nest =
  if line_number >= List.length program then (
    raise (SyntaxError (sprintf "missing function end"))
  ) else (
    let line = get_content (List.nth program line_number) in
    let line_tokens = tokenize line in
    let next_step = get_function_end program (line_number+1) in
    match line_tokens with
    | COMMAND FUNCEND :: _ -> (
      if nest = 0 then line_number else next_step (nest - 1)
    )
    | COMMAND FUNCSTART :: _ -> next_step (nest + 1)
    | _ -> next_step nest
  );;

(* Executa sequencialmente as linhas de um programa *)
let rec run_line stack scope program line_number =
  print_and_wait stack program line_number;
  (* testando se programa acabou *)
  if line_number >= List.length program || line_number = main_ret then (
    print_string ""
  ) else (
    let line = get_content (List.nth program line_number) in
    (* parsing *)
    let line_tokens = tokenize line in
    if is_function_definition line_tokens line_number then (
      (* se for definição de função, adicionar no frame
      e pular para linha após final da definição *)
      let function_end = get_function_end program (line_number+1) 0 in
      let function_id = get_function_id line_tokens in 
      let stack' = set_function stack function_id (line_number+1) in
      run_line stack' scope program (function_end+1)
    ) else (
      (* avaliação *)
      let result = _interpreter stack scope line_tokens None line_number in
      (* pilha atualizada e próxima linha de execução *)
      let stack' = fst_triple result and next_line_number = trd_triple result in
      (* execução próxima linha *)
      run_line stack' scope program next_line_number
    )
  );;

(* Função para iniciar a execução do programa *)
let run program scope = run_line new_stack scope program 0;;

(* ========== MAIN ==================================================================== *)
let get_scope scope = 
  let s = String.lowercase scope in
  match s with
  | "s" | "static" -> STATIC
  | "d" | "dynamic" -> DYNAMIC
  | _ -> DYNAMIC

let get_program file = In_channel.read_lines file;;

let command = 
  Command.basic
    ~summary: "Interpreter for a simple language with dynamic or static scope"
    Command.Spec.(
      empty
      +> anon ("filename" %: string)
      +> anon ("scopetype" %: string)
    )
    (fun filename scopetype () ->
      let scope = get_scope scopetype in
      let program = get_program filename in
      run program scope
    )

let () =
  Command.run ~version:"1.0" ~build_info:"MLP_v1.0" command;;