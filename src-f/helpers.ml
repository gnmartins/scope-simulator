open Core;;
open Types;;

(* Retorno de valores de uma tripla *)
let fst_triple = (fun (x,_,_) -> x);;
let snd_triple = (fun (_,y,_) -> y);;
let trd_triple = (fun (_,_,z) -> z);;

(* Retorna o valor de uma option *)
let get_content x =
  match x with
  | Some v -> v
  | _ -> raise RuntimeError;;

(* Pega substring terminada em `c` a partir do índice `pos` de `str` *)
let get_delimited_substr str c pos = 
  let slice = String.slice str pos (String.length str) in
  let substr_lst = String.split ~on:c slice in
  match substr_lst with
  | hd :: _ -> hd
  | _ -> raise ParsingError;; 