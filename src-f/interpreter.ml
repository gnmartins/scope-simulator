open Core;;
open Types;;
open Helpers;;
open Stacks;;

(* Aplica a operação `op` em termos de `x` e `y` *)
let operate x op y =
  match op with
  | PLUS -> x + y
  | MINUS -> x - y;;

let rec _interpreter stack scope toklist buffer line_number =
  match toklist with
  | hd :: tl -> (
    match hd with 

    | INT x -> (
      match buffer with
      | None -> _interpreter stack scope tl (Some x) line_number
      | _ -> raise (SyntaxError (sprintf "line %d" line_number))
    )

    | VAR x -> (
      match buffer with
      | None -> (
        let result = get_variable stack scope x line_number in
        _interpreter stack scope tl (Some result) line_number
      )
      | _ -> raise (SyntaxError (sprintf "line %d" line_number))
    )

    | OP op -> (
      let x = (
        match buffer with
        | Some v -> v
        | None -> 0
      ) in
      match tl with
      | INT y :: tl' -> _interpreter stack scope tl' (Some (operate x op y)) line_number
      | VAR var :: tl' -> (
        let y = get_variable stack scope var line_number in
        _interpreter stack scope tl' (Some (operate x op y)) line_number
      )
      | _ -> raise (SyntaxError (sprintf "line %d" line_number))
    )

    | COMMAND x -> (
      match x with
      | LET -> (
        match tl with
        (* `let var = expr` *)
        | VAR var :: COMMAND ASSIGN :: tl' -> (
          (* avalia o resto da expressão *)
          let evaluation = _interpreter stack scope tl' buffer line_number in
          (* pega pilha atualizada e resultado da expressão *)
          let stack = fst_triple evaluation in
          let result = snd_triple evaluation in
          (* adiciona variável na pilha *)
          let stack' = set_variable stack var result line_number in
          (* chegou ao final da expressao *)
          (stack', None, line_number+1)
        )
        | _ -> raise (SyntaxError (sprintf "line %d" line_number))
      )
      | PRINT -> (
        (* avalia o resto da expressao e printa *)
        let evaluation = _interpreter stack scope tl buffer line_number in
        let stack = fst_triple evaluation in
        let result = snd_triple evaluation in
        match result with 
        | Some v -> (
          let out = sprintf "%d" v in
          Io.print out; 
          (stack, None, line_number+1)
        )
        | None -> raise (SyntaxError (sprintf "line %d" line_number))
      )
      | CALL -> (
        match tl with
        | VAR v :: EOF :: [] -> (
          (* pega começo e fim da função no stack *)
          let (fstart, ve) = get_function stack scope v line_number in
          (* registra frame na pilha *)
          let stack' = push_frame stack (line_number + 1) ve in
          (* retorna pilha modificada e próxima linha de instrução *)
          (stack', None, fstart)
        )
        | _ -> raise (SyntaxError (sprintf "line %d" line_number))
      )
      | FUNCEND -> (
        let aux = pop_and_get_ret stack in 
        let stack' = fst aux and ret = snd aux in
        (stack', None, ret)
      )
      | _ -> raise (SyntaxError (sprintf "line %d" line_number))
    )

    | EOF -> (stack, buffer, line_number+1)
  )
  | _ -> raise (SyntaxError (sprintf "line %d" line_number))

let interpreter scope toklist = _interpreter new_stack scope toklist None 0