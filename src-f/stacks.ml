open Core;;
open Types;;
open Helpers;;

type scope = 
  | STATIC 
  | DYNAMIC;;

type variable = 
  | VARIABLE of string * int * int   (* id, valor, linha de definição *)
  | FUNCTION of string * int * int;; (* id, inicio, pai estatico *)

type frame = {ret: int; ve: int; vd: int; vars: variable list};;
let main_ret = -1;;

type stack = frame list;;

(* ---------- OPERAÇÕES COM FRAMES ---------------- *)
(* Pilha vazia *)
let new_stack = [{ret = main_ret; ve = -1; vd = -1; vars = []}];;

(* Retorna pilha com novo frame adicionado *)
let push_frame stack ret ve =
  let vd = (List.length stack) - 1 in
  stack @ [{ret = ret; ve = ve; vd = vd; vars = []}]

(* Retorna dupla com pilha sem último frame e o último frame *)
let pop_frame stack = 
  let split = List.split_n stack ((List.length stack) - 1) in
  let new_stack = fst split in 
  let frame = get_content (List.hd (snd split)) in
  (new_stack, frame);;

(* Retorna dupla com pilha sem último frame e endereço de retorno *)
let pop_and_get_ret stack =
  let split = List.split_n stack ((List.length stack) - 1) in
  let new_stack = fst split in
  let frame = get_content (List.hd (snd split)) in
  (new_stack, frame.ret);;
(* ------------------------------------------------ *)

(* ---------- SET DE VARIAVEIS NA PILHA ----------- *)
let set_var (stack : stack) (var : variable) =
  let split = pop_frame stack in
  let stack' = fst split and frame = snd split in
  stack'@[{frame with vars = frame.vars@[var]}];;

let set_variable stack variable value_option line = 
  let value = get_content value_option in
  let var = VARIABLE (variable, value, line) in
  set_var stack var

let set_function stack identifier start =
  let ve = (List.length stack) - 1 in 
  let f = FUNCTION (identifier, start, ve) in
  set_var stack f
(* ------------------------------------------------ *)

(* ---------- GET DE VARIAVEIS NA PILHA ----------- *)
(* Procura pelo `var_id` no frame *)
let get_on_frame frame var_id =
  let vars = (List.filter ~f:(fun var -> match var with
    | VARIABLE (id, _, _) when id = var_id -> true
    | FUNCTION (id, _, _) when id = var_id -> true 
    | _ -> false) frame.vars) in
  List.hd vars

(* Procura pelo `id` iterativamente em todos os frames da pilha *)
let rec get_on_stack stack scope frame_pos id = 
  if frame_pos = -1 then (
    raise (UndeclaredVariableError) 
  ) else (
    let frame_opt = List.nth stack frame_pos in
    let frame = get_content frame_opt in
    let return = get_on_frame frame id in
    match return with
    | Some x -> x
    | None -> (
      let search_next_frame = get_on_stack stack scope in
      match scope with
      | STATIC -> search_next_frame (frame.ve) id
      | DYNAMIC -> search_next_frame (frame.vd) id
    )
  )

(* Testa se a variável definida na linha `def` é acessível na linha `line` *)
let scope_test scope def line =
  match scope with
  | DYNAMIC -> true
  | STATIC -> def < line

let get_variable stack scope variable line =
  let len = List.length stack in
  let var = get_on_stack stack scope (len - 1) variable in
  match var with
  | VARIABLE (_, x, def) when (scope_test scope def line) -> x
  | _ -> raise UndeclaredVariableError

let get_function stack scope func line =
  let len = List.length stack in
  let var = get_on_stack stack scope (len - 1) func in
  match var with
  | FUNCTION (_, start, pe) when (scope_test scope (start-1) line) -> (start, pe)
  | _ -> raise UndeclaredVariableError
(* ------------------------------------------------ *)