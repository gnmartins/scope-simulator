open Core;;
open Types;;
open Helpers;;

(* Retorna uma lista com token `INT` ou uma lista vazia *)
let get_int_token str =
  if String.length str > 0 then [INT (int_of_string str)] else [];;

(* Retorna uma lista de tokens a partir de `str` *)
let rec tokenizer str buffer pos toklist = 
  (* verifica se já avaliou toda a string *)
  if pos >= String.length str then (
    let toklist' = toklist @ (get_int_token buffer) in
    toklist'@[EOF]
  ) else (
    let c = String.nget str pos in
    if Char.is_digit c then (
      (* guarda em buffer digito do número inteiro a ser tokenizado *)
      let s = Char.to_string c in
      tokenizer str (buffer^s) (pos+1) toklist
    ) else (
      (* transforma o buffer de inteiro em token *)
      let toklist' = toklist @ (get_int_token buffer) in
      let next_step = tokenizer str "" (pos+1) in (* currying *)
      match c with 
      | ' ' -> next_step toklist'
      | '+' -> next_step (toklist'@[OP PLUS])
      | '-' -> next_step (toklist'@[OP MINUS])
      | '=' -> next_step (toklist'@[COMMAND ASSIGN])
      (* pega substring até próximo espaço e cria o token adequado *)
      | _ -> (
        let piece = get_delimited_substr str ' ' pos in
        let len = String.length piece in
        let next_step = tokenizer str "" (pos+len) in
        match piece with
        | "let" -> next_step (toklist'@[COMMAND LET])
        | "print" -> next_step (toklist'@[COMMAND PRINT])
        | "function" -> next_step (toklist'@[COMMAND FUNCSTART])
        | "end" -> next_step (toklist'@[COMMAND FUNCEND])
        | "call" -> next_step (toklist'@[COMMAND CALL])
        | _ -> tokenizer str "" (pos+len) (toklist'@[VAR piece])
      )
    )
  );;

(* Açúcar sintático para função `tokenizer` *)
let tokenize str = tokenizer str "" 0 [];;
